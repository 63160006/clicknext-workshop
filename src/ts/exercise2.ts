interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];

//declare var
const mainDiv = document.getElementById('exercise2') as HTMLDivElement
const labelScore = document.createElement('div')
const para = document.createElement("p");
let score = 0;

function updateScore() {
    score += 1
    labelScore.innerText = 'Current Score: ' + score + '/10'; 
}

//assign value
labelScore.innerText = 'Current Score: ' + score + '/10';


//add info
mainDiv.appendChild(labelScore)

// main program
questions.forEach((question, index) => {
    // declare variables
    let divQuestion = document.createElement('div');
    let subDivQuestion = document.createElement('div');
    let status = document.createElement('p');
  
    // loop choices
    question.choices.forEach((choice, choiceIndex) => {
      let choicesDivQuestion = document.createElement('div');
      // declare radio button
      var radioBtn = document.createElement('input');
      radioBtn.setAttribute('type', 'radio');
      radioBtn.setAttribute('name', 'question-' + index);
      radioBtn.setAttribute('value', choiceIndex.toString());
  
      // declare span
      let text = document.createTextNode(choice);
      let spanQuestion = document.createElement('span');
      spanQuestion.appendChild(text);
  
      // append value
      choicesDivQuestion.appendChild(radioBtn);
      choicesDivQuestion.appendChild(spanQuestion);
      subDivQuestion.appendChild(choicesDivQuestion);
    });
  
    // create button
    let questionParagraph = document.createElement('p');
    let textBtn = document.createTextNode('Submit');
    let submitBtn = document.createElement('button');
    submitBtn.appendChild(textBtn);
  
    // assign value
    let text = document.createTextNode(question.question);
    // append value
    questionParagraph.appendChild(text);
    divQuestion.appendChild(questionParagraph);
    divQuestion.appendChild(subDivQuestion);
    divQuestion.appendChild(submitBtn);
    divQuestion.appendChild(status);
    mainDiv.appendChild(divQuestion);
  
    // submit button event listener
    submitBtn.addEventListener('click', () => {
      let selectedAnswer = document.querySelector('input[name="question-' + index + '"]:checked') as HTMLInputElement;;
      if (!selectedAnswer) {
        alert('Please choose an answer first!');
        return;
      }
  
      let answerIndex = parseInt(selectedAnswer.value);
      if (answerIndex === question.correctAnswer) {
        updateScore();
        status.innerText = 'Correct!';
      } else {
        status.innerText = 'Incorrect!';
      }
  
      // disable radio buttons and submit button
      let radioButtons = document.querySelectorAll('input[name="question-' + index + '"]')as NodeListOf<HTMLInputElement>;
      radioButtons.forEach((radioButton) => {
        radioButton.disabled = true;
      });
      submitBtn.disabled = true;
    });
  });
  

