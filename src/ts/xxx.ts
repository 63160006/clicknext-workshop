interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];

//declare var

const mainDiv = document.getElementById('exercise2') as HTMLDivElement;
const labelScore = document.createElement('div');
const para = document.createElement('p');
let score = 0;

function updateScore() {
    score += 1;
    labelScore.innerText = 'Current Score: ' + score + '/10';
}

labelScore.innerText = 'Current Score: ' + score + '/10';
mainDiv.appendChild(labelScore);

questions.forEach((question, index) => {
    const divQuestion = document.createElement('div');
    const subDivQuestion = document.createElement('div');
    const status = document.createElement('p');
    const choicesDiv = []; // To store the div elements for each choice

    question.choices.forEach((choice, choiceIndex) => {
        const choicesDivQuestion = document.createElement('div');
        const radioBtn = document.createElement('input');
        radioBtn.setAttribute('type', 'radio');
        radioBtn.setAttribute('name', `question${index}`); // Use name attribute to group radio buttons

        // Event listener for radio button
        radioBtn.addEventListener('click', () => {
            const selectedAnswer = parseInt(radioBtn.value, 10);
            const correctAnswer = question.correctAnswer;

            if (selectedAnswer === correctAnswer) {
                status.innerText = 'Correct!';
                updateScore();
            } else {
                status.innerText = 'Incorrect!';
            }

            // Disable radio buttons and submit button after answering
            choicesDiv.forEach((choiceDiv) => {
                choiceDiv.firstChild.disabled = true;
            });
            submitBtn.disabled = true;
        });

        radioBtn.value = choiceIndex.toString(); // Store the choice index as the value

        const text = document.createTextNode(choice);
        const spanQuestion = document.createElement('span');
        spanQuestion.appendChild(text);

        choicesDivQuestion.appendChild(radioBtn);
        choicesDivQuestion.appendChild(spanQuestion);
        subDivQuestion.appendChild(choicesDivQuestion);
        choicesDiv.push(choicesDivQuestion);
    });

    const questionParagraph = document.createElement('p');
    const textBtn = document.createTextNode('Submit');
    const submitBtn = document.createElement('button');

    submitBtn.appendChild(textBtn);

    // Event listener for submit button
    submitBtn.addEventListener('click', () => {
        const selectedAnswer = parseInt(
            document.querySelector(`input[name="question${index}"]:checked`)?.value,
            10
        );

        if (isNaN(selectedAnswer)) {
            alert('Please choose an answer first!');
            return; // Do nothing if no answer is selected
        }

        const correctAnswer = question.correctAnswer;

        if (selectedAnswer === correctAnswer) {
            status.innerText = 'Correct!';
            updateScore();
        } else {
            status.innerText = 'Incorrect!';
        }

        // Disable radio buttons and submit button after answering
        choicesDiv.forEach((choiceDiv) => {
            choiceDiv.firstChild.disabled = true;
        });
        submitBtn.disabled = true;
    });

    const text = document.createTextNode(question.question);

    questionParagraph.appendChild(text);
    divQuestion.appendChild(questionParagraph);
    divQuestion.appendChild(subDivQuestion);
    divQuestion.appendChild(submitBtn);
    divQuestion.appendChild(status);
    mainDiv.appendChild(divQuestion);
});
