// const inputNumber = document.getElementById('inputNumber') as HTMLInputElement
// const equalButton = document.getElementById('equalButton') as HTMLButtonElement
// const notEqualButton = document.getElementById('notEqualButton') as HTMLButtonElement
// const greaterThanButton = document.getElementById('greaterThanButton') as HTMLButtonElement
// const greaterThanOrEqualButton = document.getElementById('greaterThanOrEqualButton') as HTMLButtonElement
// const output = document.getElementById('output') as HTMLParagraphElement

// equalButton.addEventListener('click', function () {
//     const value: number = +inputNumber.value
//     const result = value === 10
//     output.innerText = result + ''
// });
// notEqualButton.addEventListener('click', function () {
//     const value: number = +inputNumber.value
//     const result = value != 10
//     output.innerText = result + ''
// });
// greaterThanButton.addEventListener('click', function () {
//     const value: number = +inputNumber.value
//     const result = value > 10
//     output.innerText = result + ''
// });
// greaterThanOrEqualButton.addEventListener('click', function () {
//     const value: number = +inputNumber.value
//     const result = value >= 10
//     output.innerText = result + ''
// });

// const person = {
//     name: 'Jonh Doe',
//     age: 25,
//     address: {
//         street: '123 Main St',
//         city: 'Cityville',
//         country: 'Countryland'
//     },
//     hobbies: ['coding', 'reading', 'music']
// }

// console.log(person.address.country)

interface Address {
    street: string
    city: string
    country: string
}
interface Person {
    name: string
    age: number
    address: Address
    hobbies: string[]
}
let person: Person = {
    name: 'Jonh Doe',
    age: 25,
    address: {
        street: '123 Main St',
        city: 'Cityville',
        country: 'Countryland'
    },
    hobbies: ['coding', 'reading', 'music']
}
let address: Address = {
    city: 'abc',
    street: 'def',
    country: 'asd',
}


const n: number = 10
const s: string = 'text'
const b: boolean = true
const array: number[] = [1,2,3]
const array2 = [1,'2',3,true]
const array3: boolean[] = [true, false]

//Java
// int add(int number1, int number2) { 

// }

// function log(message: string): void {
//     return
// }

function add(number1: number, number2: number): number {
    return number1 + number2
}

function DisplayDetail(person: Person): string {
    return 'name: ' + person.name + '; age: ' + person.age + '; address: ' + person.address.street 
    + ' '+ person.address.city + ' ' + person.address.country + '; hobbies: ' + person.hobbies.join(',')     
}

const addresses: Address[] = [
    {street: 'a', city: 'b', country: 'c'},
    {street: 'a', city: 'b', country: 'c'},
    {street: 'a', city: 'b', country: 'c'},
    {street: 'a', city: 'b', country: 'c'},
]

console.log(DisplayDetail(person))

export{}